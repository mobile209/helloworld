import 'package:flutter/material.dart';


void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

String englistGreeting="Hello Flutter";
String spanishGreeting ="Hola Flutter";
String japaneseGreeting="こんにちはフラッター";
String indonesianGreeting="Halo Flutter";
String laoGreeting="ສະບາຍດີ flutter";
String nepalGreeting="नमस्ते फडफड";

class _MyStatefulWidgetState extends State<HelloFlutterApp>{
  String displayText = englistGreeting;
  String displayText1 =japaneseGreeting;
  String displayText2= spanishGreeting;
  @override
   Widget build(BuildContext context){
     return MaterialApp(
       debugShowCheckedModeBanner: false,
         home: Scaffold(
           appBar: AppBar(
             title: Text(
              "Title"
            ),
           leading: Icon(Icons.home),
          actions: <Widget>[
              IconButton(
                  onPressed: (){
                    setState(() {
                      displayText = displayText!=japaneseGreeting?
                      japaneseGreeting : englistGreeting;
                    });
                  },
                  icon: Icon(Icons.dangerous)),
              IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText!=spanishGreeting?
                    spanishGreeting : laoGreeting;
                  });
                },
                icon: Icon(Icons.face_sharp)),
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText!=indonesianGreeting?
                    indonesianGreeting : nepalGreeting;
                  });
                },
                icon: Icon(Icons.kayaking)),
            ],
          ),

          body: Center(
            child: Text(
              displayText,
              style: TextStyle(fontSize: 26),
            ),
          ),
        )
    );
  }
}


























// class HelloFlutterApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context){
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//         home: Scaffold(
//           appBar: AppBar(
//             title: Text(
//               "Title"
//             ),
//             leading: Icon(Icons.home),
//             actions: <Widget>[
//               IconButton(onPressed: (){}, icon: Icon(Icons.refresh))
//             ],
//           ),
//           body: Center(
//             child: Text(
//               "Hello Flutter",style: TextStyle(fontSize: 26),
//             ),
//           ),
//         )
//     );
//   }
// }